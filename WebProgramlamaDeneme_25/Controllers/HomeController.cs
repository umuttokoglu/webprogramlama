﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebProgramlamaDeneme_25.Models;

namespace WebProgramlamaDeneme_25.Controllers
{
    public class HomeController : Controller
    {
        private ProjeDBEntities db = new ProjeDBEntities();


        // GET: Makales
        public ActionResult Anasayfa()
        {
            var makale = db.Makale.Include(m => m.Uye);
            return View(makale.OrderByDescending(i => i.Tarih).Take(5).ToList());
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}