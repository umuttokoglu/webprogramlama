﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebProgramlamaDeneme_25.Models;

namespace WebProgramlamaDeneme_25.Controllers
{
    public class MakalesController : Controller
    {
        private ProjeDBEntities db = new ProjeDBEntities();

        // GET: Makales
        public ActionResult Ekranayaz()
        {
            var makale = db.Makale.Include(m => m.Uye);
            return View(makale.ToList());
        }

        // GET: Makales/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Makale makale = db.Makale.Find(id);
            if (makale == null)
            {
                return HttpNotFound();
            }
            return View(makale);
        }

        // GET: Makales/Create
        public ActionResult Create()
        {
            ViewBag.UyeId = new SelectList(db.Uye, "UyeId", "KullaniciAdi");
            return View();
        }

        // POST: Makales/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MakaleId,UyeId,Baslik,İcerik,Tarih,Saat")] Makale makale)
        {
            if (ModelState.IsValid)
            {
                db.Makale.Add(makale);
                db.SaveChanges();
                return RedirectToAction("Ekranayaz");
            }

            ViewBag.UyeId = new SelectList(db.Uye, "UyeId", "KullaniciAdi", makale.UyeId);
            return View(makale);
        }

        // GET: Makales/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Makale makale = db.Makale.Find(id);
            if (makale == null)
            {
                return HttpNotFound();
            }
            ViewBag.UyeId = new SelectList(db.Uye, "UyeId", "KullaniciAdi", makale.UyeId);
            return View(makale);
        }

        // POST: Makales/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "MakaleId,UyeId,Baslik,İcerik,Tarih,Saat")] Makale makale)
        {
            if (ModelState.IsValid)
            {
                db.Entry(makale).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Ekranayaz");
            }
            ViewBag.UyeId = new SelectList(db.Uye, "UyeId", "KullaniciAdi", makale.UyeId);
            return View(makale);
        }

        // GET: Makales/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Makale makale = db.Makale.Find(id);
            if (makale == null)
            {
                return HttpNotFound();
            }
            return View(makale);
        }

        // POST: Makales/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Makale makale = db.Makale.Find(id);
            db.Makale.Remove(makale);
            db.SaveChanges();
            return RedirectToAction("Ekranayaz");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
